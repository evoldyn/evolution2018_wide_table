# Evolutionary Biology – Montpellier 2018 

## Full online static representation in a wide table format (web page and pdf)

This repository/page is not affiliated in any way with the [Second Joint Congress on Evolutionary Biology – Montpellier 2018](https://www.evolutionmontpellier2018.org/home)

It is merely a different static representation in a wide table format of the [full online program](https://www.evolutionmontpellier2018.org/program-second-joint-congress-evolutionary-biology).

You can also the open full official [online program in a new window](https://interactive-programme.europa-organisation.com/evolBiology2018/programme.php?pgm=evolBiology2018)

## Wide table Evolution 2018 program → [web page](https://evoldyn.gitlab.io/evolution2018_wide_table/) 

You can also [download a pdf-file (820 kb)](https://evoldyn.gitlab.io/evolution2018_wide_table/Evolution_2018_wide_table.pdf) of the Evolution 2018 program in wide table format in this git repository or from the accompanying web page.