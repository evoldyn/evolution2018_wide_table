"use strict";
main();

function main(argument) {
  if (!$("#by_table").length) {
    $("body").append(`<div id="by_table"></div>`);
  }

  if (!$("#by_table_styles").length) {
    $("body").append(`<style id="by_table_styles">${ret_styles()}</style>`);
  }

  $(".sessionContainer").each(function(index, el) {
    let ori_sdate = $(el).find(".sessionDate");
    let ori_stime = $(el).find(".sessionTime");
    let ori_room  = $(el).find(".sessionRoom");

    let sdate = ori_sdate.clone();
    let stime = ori_stime.clone();

    let datid = sdate.text().replace(/[^a-z0-9]+/gi, "_");
    let timed = stime.text().replace(/[^a-z0-9]+/gi, "_");

    let dtuid = datid + "_" + timed;
    console.log(dtuid);

    let ori_stitl = $(el).find(".sessionTitle");
    let ori_sessi = $(el).find(".BodySession");

    let stitl = ori_stitl.clone();
    let sessi = ori_sessi.clone();
    let sroom = ori_room.clone();

    if (!$("#wrap_" + dtuid).length) {
      $("div#by_table").append(wrapper_shim(dtuid));
      $("#wrap_" + dtuid).append(sdate);
      $("#wrap_" + dtuid).append(stime);
      $("#wrap_" + dtuid).append(table_shim(dtuid));
    }

    td_filler(dtuid, stitl, sessi, sroom);
  });
}

function td_filler(uid, th_td, td, sroom) {
  // let content_th_td = `<td></td>`;
  // let content_td = `<td></td>`;
  // $("#thead_tr_" + uid).append(content_th_td);
  // $("#tbody_tr_" + uid).append(content_td);

  let content_th_td = $("#thead_tr_" + uid).append(`<td></td>`);
  let content_td = $("#tbody_tr_" + uid).append(`<td></td>`);
  content_th_td.children("td").last().append(th_td);
  content_td.children("td").last().append(td);
  content_th_td.children("td").last().append(sroom);
}

function wrapper_shim(uid) {
  let wrp_html = `
  <div id="wrap_${uid}"" class="wrapper_shim">
  </div>
  `;
  return wrp_html;
}

function table_shim(uid) {
  let tbl_html = `
  <table id="${uid}" class="table table-hover">
    <thead>
      <tr id="thead_tr_${uid}">
        
      </tr>
    </thead>
    <tbody>
      <tr id="tbody_tr_${uid}">
        
      </tr>
    </tbody>
  </table>
  `;
  return tbl_html;
}

function ret_styles() {
  let tbl_styles=`
  div#by_table {
    width: 7800px;
  }

  div#by_table tr td {
    width: 600px;
  }

  div#by_table .interventiontime,
  div#by_table .interventionTime {
    width: 60px;
    margin-right: -10px;
  }

  div#by_table td {
    vertical-align: top;
    padding: 0;
    margin: 0;
  }

  div#by_table p.sessionTitle {
    margin: 0;
    padding-left: 60px;
    text-transform: unset;
    white-space: normal;
    /* width: 300px; */
  }

  div#by_table .sessionModerators {
    opacity: 0.7;
    margin: 0;
    padding: 0;
    margin-left: 60px;
  }

  div#by_table .sessionSpeakers {
    opacity: 0.55;
    margin: 0;
    padding: 0;
    margin-left: 20px;
  }

  div#by_table .sessionSponsor,
  div#by_table .fa-clock-o {
    display: none;
  }

  div#by_table .interventionTitle {
    margin: 0;
    padding: 0;
    border-left: none;
  }

  span.interventionTitle:before {
    content: "";
    height: 20px;
    position: absolute;
    top: 22px;
    left: -7px;
    border-left: 3px solid #fd9c5c;
  }

  div#by_table .moderator_role:before,
  div#by_table .sessionTitle:before {
    content: none;
  }

  div#by_table .sessionTime,
  div#by_table .sessionDate {
    display: inline-block;
    font-size: 24px;
    margin: 0;
    padding: 10px 10px;
    border-top: 3px solid #dddd;
  }

  div#by_table p.sessionRoom {
    text-align: left !important;
    padding: 0;
    margin: 0;
    padding-left: 60px;
    color: hsla(25, 50%, 50%, 0.95) !important;
    font-weight: 600;
  }
`
  return tbl_styles;
}